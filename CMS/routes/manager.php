<?php
/**
 * Routes Configuration
 * User: Marc Newton (hello@marcnewton.co.uk)
 * Date: 10/06/2017
 * Time: 11:21 AM
 */

use Illuminate\Support\Facades\Route;

Route::group([
		'domain'        => env('APP_URL'),
		'namespace'     => 'Oteck\Ecommerce\CMS\Http\Controllers',
		'middleware'    => ['web']
	], function() {

	// Authentication Routes...
	Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
	Route::post('login', 'Auth\LoginController@login');
	Route::post('logout', 'Auth\LoginController@logout')->name('logout');

	// Password Reset Routes...
	Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
	Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
	Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
	Route::post('password/reset', 'Auth\ResetPasswordController@reset');

	// Private routes
	Route::group([
		'middleware' => ['auth'] # TODO Add middleware "permissions"
	], function() {

		// Registration Routes...
		Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
		Route::post('register', 'Auth\RegisterController@register');

		Route::get('/', function () {
			return view('manager::dashboard.index');
		});

		Route::get('/sash', function () {
			return view('manager::dashboard.index');
		});

	});
});

