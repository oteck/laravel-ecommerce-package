/**
 * Created by Marc Newton  on 11/07/2017.
 */

(function($){

	var navigation = function()
	{
		var self = this;

		self.sidebarObject = $('#sidebar-wrapper');
		self.dashboardObject = $('#dashboard');

		self.sidebarObject.on('click', 'a', function (e) {

			var data = $.extend({
				action: false
			}, $(this).data());

			if(data.action === false) {
				return true;
			}

			e.preventDefault();

			if(!$.isFunction(navigation.prototype[data.action])) {
				// TODO : JavaScript error alert - Function not found.
				return false;
			}

			self[data.action](self);

		});
	};

	navigation.prototype.dashboard = function(self)
	{
		this.dashboardObject.toggleDisplay('dashboard').one('click', '.close', function() {
			self.dashboardObject.toggleDisplay('dashboard');
		});
	};

	$(document).ready(function() {

		new navigation();

	});

})(jQuery);