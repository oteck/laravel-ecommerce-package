/**
 * Created by marc on 13/07/2017.
 */

var eventIs = function()
{
	this.element = document.createElement('animator');

	this.event = {
		animation: null,
		transition: null
	};
};

eventIs.prototype.animation = function()
{
	if(this.event.animation !== null) {
		return this.event.animation;
	}

	var key;

	var animations = {
		animation: 'animationend',
		OAnimation: 'oAnimationEnd',
		MozAnimation: 'animationend',
		WebkitAnimation: 'webkitAnimationEnd'
	};

	for (key in animations){
		if (this.element.style[key] !== undefined) {
			this.event.animation = animations[key];
			break;
		}
	}

	return this.event.animation;
};

eventIs.prototype.transition = function()
{
	if(this.event.transition !== null) {
		return this.event.transition;
	}

	var key;

	var transitions = {
		transition: 'transitionend',
		OTransition: 'oTransitionEnd',
		MozTransition: 'transitionend',
		WebkitTransition: 'webkitTransitionEnd'
	};

	for (key in transitions){
		if (this.element.style[key] !== undefined) {
			this.event.transition = transitions[key];
			break;
		}
	}

	return this.event.transition;
};

jQuery.eventIs = new eventIs();

/**
 *
 * @param animation
 * @returns {jQuery}
 */
jQuery.fn.toggleDisplay = function()
{
	var self = this;

	// Play forward animation
	if(!this.hasClass('display')) {

		this.addClass('animated display');

		this.one(jQuery.eventIs.animation(), function() {
			self.removeClass('animated');
		});

		return this;
	}

	// Play reverse animation
	this.one(jQuery.eventIs.animation(), function() {
		self.removeClass('animated reverse display');
	}).addClass('animated reverse');

	return this;
};