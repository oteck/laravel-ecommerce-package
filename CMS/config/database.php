<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Default Database Connection Name
	|--------------------------------------------------------------------------
	|
	| Here you may specify which of the database connections below you wish
	| to use as your default connection for all database work. Of course
	| you may use many connections at once using the Database library.
	|
	*/

	'default' => env('DB_CONNECTION', 'master'),

	/*
	|--------------------------------------------------------------------------
	| Database Connections
	|--------------------------------------------------------------------------
	|
	| Here are each of the database connections setup for your application.
	| Of course, examples of configuring each database platform that is
	| supported by Laravel is shown below to make development simple.
	|
	|
	| All database work in Laravel is done through the PHP PDO facilities
	| so make sure you have the driver for your particular database of
	| choice installed on your machine before you begin development.
	|
	*/

	'connections' => [

		'master' => [
			'driver'        => 'mysql',
			'host'          => '127.0.0.1',
			'port'          => '3306',
			'database'      => 'cms_master',
			'username'      => 'root',
			'password'      => '',
			'unix_socket'   => '',
			'charset'       => 'utf8',
			'collation'     => 'utf8_unicode_ci',
			'prefix'        => '',
			'strict'        => true,
			'engine'        => null,
		],

		'shop' => [
			'driver'        => 'mysql',
			'host'          => '127.0.0.1',
			'port'          => '3306',
			'database'      => '',
			'username'      => 'root',
			'password'      => '',
			'unix_socket'   => '',
			'charset'       => 'utf8',
			'collation'     => 'utf8_unicode_ci',
			'prefix'        => '',
			'strict'        => true,
			'engine'        => null,
		],

		'warehouses' => [
			'driver'        => 'mysql',
			'host'          => '127.0.0.1',
			'port'          => '3306',
			'database'      => 'cms_warehouses',
			'username'      => 'root',
			'password'      => '',
			'unix_socket'   => '',
			'charset'       => 'utf8',
			'collation'     => 'utf8_unicode_ci',
			'prefix'        => '',
			'strict'        => true,
			'engine'        => null,
		],

		'suppliers' => [
			'driver'        => 'mysql',
			'host'          => '127.0.0.1',
			'port'          => '3306',
			'database'      => '',
			'username'      => 'root',
			'password'      => '',
			'unix_socket'   => '',
			'charset'       => 'utf8',
			'collation'     => 'utf8_unicode_ci',
			'prefix'        => '',
			'strict'        => true,
			'engine'        => null,
		],
	],

];