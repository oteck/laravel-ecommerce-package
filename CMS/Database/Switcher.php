<?php
/**
 * Created by PhpStorm.
 * User: Marc Newton (hello@marcnewton.co.uk)
 * Date: 25/06/2017
 * Time: 9:48 PM
 */

namespace CMS\Database;

use Illuminate\Support\Facades\{
	Config, DB
};

class Switcher {

	/**
	 * @param String $schema
	 * @return bool
	 */
	public static function shop($schema = '')
	{
		if(empty($schema))
			$schema = 'shop_retail';

		Config::set('database.connections.shop.database', $schema);

		DB::purge('shop');

		DB::connection('shop')->reconnect();

		return ($schema === DB::connection('shop')->getDatabaseName());
	}

	/**
	 * @return bool
	 */
	public static function master()
	{
		$schema = 'cms_master';

		Config::set('database.connections.master.database', $schema);

		DB::purge('master');

		DB::connection('master')->reconnect();

		return ($schema === DB::connection('master')->getDatabaseName());
	}

	/**
	 * @return bool
	 */
	public static function warehouses()
	{
		$schema = 'cms_warehouses';

		Config::set('database.connections.warehouses.database', $schema);

		DB::purge('warehouses');

		DB::connection('warehouses')->reconnect();

		return ($schema === DB::connection('warehouses')->getDatabaseName());
	}

	/**
	 * @return bool
	 */
	public static function suppliers()
	{
		$schema = 'cms_suppliers';

		Config::set('database.connections.suppliers.database', $schema);

		DB::purge('suppliers');

		DB::connection('suppliers')->reconnect();

		return ($schema === DB::connection('suppliers')->getDatabaseName());
	}
}