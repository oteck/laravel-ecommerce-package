<?php
/**
 * Created by PhpStorm.
 * User: Marc Newton (hello@marcnewton.co.uk)
 * Date: 10/06/2017
 * Time: 11:32 AM
 */

namespace CMS\Model;


use Illuminate\Database\Eloquent\Model;

class Country extends Model
{

	protected $connection = 'master';

	/**
	 * Load single country data.
	 *
	 * @param integer|string $key Fetch county using ID | ISO | CODE
	 */
	public function single($key)
	{

	}
}