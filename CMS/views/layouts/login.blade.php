<!DOCTYPE html>

<html lang="en">
<head>
	@include('manager::layouts.partials.meta')
	<link rel="stylesheet" href="{{ asset('vendor/ecommerce/css/login.css') }}" type="text/css" />
	@yield('head-styles')

	@yield('head-scripts')
</head>
<body>

<div id="app">
	@yield('content')
</div>

</body>
<script src="{{ asset('js/app.js') }}"></script>
</html>