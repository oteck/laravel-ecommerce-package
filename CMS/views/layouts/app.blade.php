<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}">
<head>
	@include('manager::layouts.partials.meta')
	<link rel="stylesheet" href="{{ asset('vendor/ecommerce/css/manager.css') }}" type="text/css" />
	@yield('head-styles')

	<script type="application/javascript" src="{{ asset('vendor/ecommerce/js/jquery-3.2.1.min.js') }}"></script>
	<script type="application/javascript" src="{{ asset('vendor/ecommerce/js/jquery-migrate-3.0.0.min.js') }}"></script>
	<script type="application/javascript" src="{{ asset('vendor/ecommerce/js/jquery-extensions.min.js') }}"></script>
	<script type="application/javascript" src="{{ asset('vendor/ecommerce/js/navigation.min.js') }}"></script>
	@yield('head-scripts')
</head>
<body>

	@yield('body-scripts')

	@include('manager::layouts.partials.dashboard')

	<div id="app">
		<div id="sidebar-wrapper">
			@yield('navigation')
		</div>
		<div id="content-wrapper">
			@yield('content')
		</div>
	</div>

	@yield('footer-scripts')
</body>

</html>