<?php
/**
 * Created by PhpStorm.
 * User: Marc Newton (hello@marcnewton.co.uk)
 * Date: 13/06/2017
 * Time: 9:49 PM
 */


/**
 * Convert a formatted string or flat array list of strings into system friendly string(s).
 * Returns input(s) formatted to lowercase and none alphanumerical to single dashes between valid chars.
 *
 * @link http://php.net/manual/en/function.preg-replace.php - Refrence for return type.
 *
 * @param $subject array|string String or flat array of strings, Function operation will occur on every entry of the array.
 * @param $separator string default (-)
 * @param $extended bool Allow extended character sets (Cyrillic, Umlaut etc)
 *
 * @return mixed Returns $subject as input array|string or null on fail.
 */
function systematize_string($subject, $separator = '-', $extended = false)
{
	# TODO MEDIUM : Test international alphanumerics ($extended = true)
	# ABCD àáâäãå EFGH èéêë IJKL ìíîï MNOP òóôöõø QRST ùúûü UVWX ÿýñç YZ čšž 12 ÀÁÂÄÃÅ 24 ÈÉÊË 56 ÌÍÎÏ 78 ÒÓÔÖÕØ 90 ÙÚÛÜŸÝÑßÇŒÆČŠŽ∂ð

	$pattern = $extended ? '/[^a-z0-9\pL]/' : '/[^a-z0-9]/';

	$subject = preg_replace($pattern, $separator, strtolower(trim($subject)));

	$subject = preg_replace('/\\' . $separator . '+/', $separator, $subject);

	return trim($subject, $separator);
}

