<?php
/**
 * Copyright (c) 2016 Mathew Berry (contact@mathewberry.com), Marc Newton (hello@marcnewton.co.uk)
 */

namespace CMS\Console\Commands;

use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\{
	DB, Password
};

class SetupCMS extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'setup:cms';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Run through setup process';

	/**
	 * Get the registration rules.
	 *
	 * @return array
	 */
	protected function rules()
	{
		return [
			'name' => 'required|string|min:2|max:32',
			'email' => 'required|string|email|max:64|unique:users',
			'password' => 'required|string|min:6|confirmed',
		];
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function handle()
	{

// Default system command properties
//		array:8 [
//		  "help" => false
//		  "quiet" => false
//		  "verbose" => false
//		  "version" => false
//		  "ansi" => false
//		  "no-ansi" => false
//		  "no-interaction" => false
//		  "env" => null
//		]

		$exists = DB::table('users')->count();

		if($exists > 0)
		{
			$this->alert('Sorry, This command can only be run on first time installation!');
			return;
		}

		# TODO LOW : Uncomment confirm input.
		#$this->confirm('Do you wish to go through the setup process now?');

		# Ask for name

		$name = false;

		while($name === false)
		{
			$response = $this->ask('What is your name?');

			if(strlen($response) < 2)
			{
				$this->alert('I am sure you are holding back on me, What do I really call you?');
				continue;
			}

			$name = $response;
			break;
		}

		# Ask for e-mail and generate password

		$email = false;

		while($email === false)
		{
			$response = trim($this->ask('What is your e-mail address?'));

			# Validate input

			if(!filter_var($response, FILTER_VALIDATE_EMAIL))
			{
				$this->alert('Invalid E-Mail address!');
				continue;
			}

			# Validate confirmation input

			$confirm = trim($this->ask('Confirm your e-mail address'));

			if(!filter_var($confirm, FILTER_VALIDATE_EMAIL))
			{
				$this->alert('Invalid E-Mail address!');
				continue;
			}

			# Compare e-mail inputs

			if($response !== $confirm)
			{
				$this->alert('Your e-mail address confirmation did not match your original submission!');
				continue;
			}

			$email = $response;
			break;
		}

		# Register account;

		$input = [
			'name' => $name,
			'email' => $email,
			'password' => bcrypt(str_random(16))
		];

		try {
			$user = User::create($input);
		} catch (\PDOException $e) {
			$this->alert('Failed to create new user, Check /storage/logs for information!');
			return;
		}

		if(empty($user->id)) {
			$this->alert('Unknown error, Could not confirm a valid user was created! Check /storage/logs for any information!');
			return;
		}

		Password::broker()->sendResetLink(['email' => $email]);

		if(!Password::RESET_LINK_SENT)
		{
			$this->alert('Failed to send a password reset confirmation e-mail to ' . $email);
			return;
		}

		$this->line('The setup is now complete...');
		$this->line('Please check ' . $email . ' for an e-mail to set your password!');

		return;
	}

}
