<?php
/**
 * Copyright (c) 2016 Mathew Berry (contact@mathewberry.com), Marc Newton (hello@marcnewton.co.uk)
 */

namespace CMS\Console\Commands;

use CMS\Database\Switcher;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\{
	App, DB, Schema
};

class MigrateMaster extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'migrate:master {--patch}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Creates or patches the cms master database';

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function handle()
	{

// Default system command properties
//		array:8 [
//		  "help" => false
//		  "quiet" => false
//		  "verbose" => false
//		  "version" => false
//		  "ansi" => false
//		  "no-ansi" => false
//		  "no-interaction" => false
//		  "env" => null
//		]

//		$option = $this->options();

		$schema = 'cms_master';

		$exists = DB::select('SELECT `SCHEMA_NAME` FROM `information_schema`.`SCHEMATA` WHERE `SCHEMA_NAME` = :schema', [':schema' => $schema]);

		if(empty($exists))
		{
			$result = DB::statement('CREATE DATABASE `' . $schema .'` CHARACTER SET `utf8` COLLATE `utf8_general_ci`');

			if($result !== true) {
				$this->alert('Failed to create database ' . $schema);
				return;
			}

			$this->line('Created schema ' . $schema);
		}

		if(!Switcher::master())
		{
			$this->line('Failed to switch to schema :: ' . $schema);
			$this->line('Master migration has been aborted!');
			return;
		}

		$this->line('Connected to ' . $schema);

		$path = MIGRATIONS_CMS . ( $this->option('patch') ? 'patch' . DIRECTORY_SEPARATOR : '');

		$this->line('Running migrations from :: ' . $path);

		# Create migrations table

		if(!Schema::hasTable('migrations'))
			App::make('migration.repository')->createRepository();

		# Run Migrations

		$migrator = App::make('migrator');
		$migrator->run($path);
	}

}
