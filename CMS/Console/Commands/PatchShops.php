<?php
/**
 * Copyright (c) 2016 Mathew Berry (contact@mathewberry.com), Marc Newton (hello@marcnewton.co.uk)
 */

namespace CMS\Console\Commands;

use CMS\Database\Switcher;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\{
	App, Artisan, DB
};

class PatchShops extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'patch:shops';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Updates all shops';

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function handle()
	{

// Default system command properties
//		array:8 [
//		  "help" => false
//		  "quiet" => false
//		  "verbose" => false
//		  "version" => false
//		  "ansi" => false
//		  "no-ansi" => false
//		  "no-interaction" => false
//		  "env" => null
//		]

//		$option = $this->options();

		$schemas = DB::select('SELECT `SCHEMA_NAME` FROM `information_schema`.`SCHEMATA` WHERE `SCHEMA_NAME` LIKE :schema', [':schema' => 'shop_%']);

		if(!empty($schemas))
		{
			foreach ($schemas AS $schema)
			{
				$this->line('Switching to ' . $schema);

				Switcher::shop($schema);

				$current = DB::connection()->getDatabaseName();

				if($current !== $schema)
				{
					$this->alert('Failed to switch to ' . $schema . ' from ' . $current);
					break;
				}

				$this->line('Connected to ' . $schema . '... Starting migration patch...');

				# Run Migrations

				$migrator = App::make('migrator');
				$migrator->run( MIGRATIONS_SHOP . 'patch' . DIRECTORY_SEPARATOR );
			}

			return;
		}

		$this->alert('Failed to create new shop, A database schema called [%s] already exists.', $schema);
	}

}
