<?php
/**
 * Copyright (c) 2016 Mathew Berry (contact@mathewberry.com), Marc Newton (hello@marcnewton.co.uk)
 */

namespace CMS\Console\Commands;

use CMS\Database\Switcher;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\{
	App, Artisan, DB, Schema
};

class MakeShop extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'make:shop {schema}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Creates a new shop';

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function handle()
	{
		$schema = systematize_string($this->argument('schema'), '_');

// Default system command properties
//		array:8 [
//		  "help" => false
//		  "quiet" => false
//		  "verbose" => false
//		  "version" => false
//		  "ansi" => false
//		  "no-ansi" => false
//		  "no-interaction" => false
//		  "env" => null
//		]

//		$option = $this->options();

		if (empty($schema)) {
			$this->alert('Invalid schema name, only latin chars ( a-z ) and underscores ( _ ) are accepted!');
			return;
		}

		$schema = 'shop_' . $schema;

		$exists = DB::select('SELECT `SCHEMA_NAME` FROM `information_schema`.`SCHEMATA` WHERE `SCHEMA_NAME` = :schema', [':schema' => $schema]);

		if(empty($exists))
		{
			$result = DB::statement('CREATE DATABASE `' . $schema . '` CHARACTER SET `utf8` COLLATE `utf8_general_ci`');

			if($result !== true) {
				$this->alert('Failed to create database ' . $schema);
				return;
			}

			$this->line('Created schema ' . $schema);

			Switcher::shop($schema);

			$this->line('Connected to ' . $schema . '... Starting migration');

			# Create migrations table

			if(!Schema::hasTable('migrations'))
				App::make('migration.repository')->createRepository();

			# Run Migrations

			$migrator = App::make('migrator');
			$migrator->run(MIGRATIONS_SHOP);

			return;
		}

		$this->alert('Failed to create new shop, A database schema called [%s] already exists.', $schema);
	}

}
