<?php
/**
 * Created by PhpStorm.
 * User: Marc Newton (hello@marcnewton.co.uk)
 * Date: 11/06/2017
 * Time: 9:35 PM
 */

namespace Shop\Model;


use Illuminate\Database\Eloquent\{Builder, Model};

class Tag extends Model
{
	/**
	 * The connection name for the model.
	 *
	 * @var string
	 */
	protected $connection = 'shop';

	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'tags';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [];

	/**
	 * Tags assignable to any resource.
	 *
	 * @param $model
	 * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
	 */
	public function tagged($model)
	{
		return $this->morphedByMany($model, 'tag_polymorphic');
	}
}