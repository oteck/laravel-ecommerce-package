<?php
/**
 * Created by PhpStorm.
 * User: Marc Newton (hello@marcnewton.co.uk)
 * Date: 11/06/2017
 * Time: 9:35 PM
 */

namespace Shop\Model;


use Illuminate\Database\Eloquent\Model;

class Media extends Model
{

	protected $connection = 'shop';

}