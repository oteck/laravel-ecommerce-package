## Requirements ##

* [PHP >= 7.1](Link http://php.net)

## Documentations ##

## Public Assets ##

Before installing this package, The following public assets directory will be created in your laravel **~root/public** folder on installation of this package:

```
#!comment
/public/vendor/ecommerce

```
Ensure there are no conflicting sub directories to avoid assets being overwritten by this package!

## Installation ##


```
#!composer

$ composer require oteck/ecommerce:^1.0
```

https://packagist.org/packages/oteck/ecommerce


## Configuration ##

### Environment configurations (.env) ###
Configure the URL to access the CMS Manager.
```
#!php

APP_URL=https://manager.example.com
```

The connection must be set to **master**.
```
#!php

DB_CONNECTION=master
```

Leave the database value blank, We only need the connection information. No default schema name is required here.
```
#!php

DB_DATABASE=
```

### Debugging Mode ###

To enable debugging mode, set **APP_DEBUG=true** in your .env file.

**IMPORTANT:** Please make sure that APP_DEBUG is set to false when your app is on production.

### Service Provider ###
In **/config/app.php** add the following class under the "**providers**" key where the comment heading "**Package Service Providers...**" is written:

```
#!php

Oteck\Ecommerce\CmsServiceProvider::class
```

## First Time Setup ##

Using terminal execute the following command in the root directory of your laravel installation:


```
#!artisan

php artisan setup:cms
```
Follow the on screen instructions to configure the CMS.

## Contributing ##

## Security ##

## Credits ##

## License ##