<?php

namespace Oteck\Ecommerce;

use CMS\Database\Switcher;
use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Foundation\Application;

class CmsServiceProvider extends ServiceProvider
{
	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	protected $inCms = true;

	public function __construct(Application $app)
	{
		parent::__construct($app);

		/*
	     * Check if the request is for the CMS or a shop.
	     */
		$this->inCms = ( url('/') == config('app.url') );
	}

	/**
     * Bootstrap the application services.
     * @link https://laravel.com/docs/5.0/packages#views
     *
     * Set APP_URL in .env or 'url' in root/config/app.php for CMS domain name.
     *
     * @return void
     */
    public function boot ()
    {
	    /*
		 * Defined the base path to the package root.
		 */
	    if(!defined('PACKAGE_BOOTED'))
	    {
		    define('PACKAGE_BOOTED', true);

		    define('PACKAGE_PATH', __DIR__ . DIRECTORY_SEPARATOR);

		    define('PACKAGE_PATH_CMS', PACKAGE_PATH . 'CMS' . DIRECTORY_SEPARATOR);

		    define('PACKAGE_PATH_SHOP', PACKAGE_PATH . 'Shop' . DIRECTORY_SEPARATOR);

		    define('MIGRATIONS_CMS', PACKAGE_PATH . 'migrations' . DIRECTORY_SEPARATOR . 'cms' . DIRECTORY_SEPARATOR);

		    define('MIGRATIONS_SHOP', PACKAGE_PATH . 'migrations' . DIRECTORY_SEPARATOR . 'shop' . DIRECTORY_SEPARATOR);
	    }

	    /*
		|--------------------------------------------------------------------------
		| Always
		|--------------------------------------------------------------------------
		|
		| Run code for both shops and cms
	    |
		*/

	    require PACKAGE_PATH_CMS . 'Helpers/functions.php';

	    /*
		|--------------------------------------------------------------------------
		| CMS Only
		|--------------------------------------------------------------------------
		|
		| Only run if the current request url root matches the main configured application url
	    | See APP_URL in .env or 'url' in root/config/app.php
		|
		*/

	    if($this->inCms === true)
	    {
		    $this->publishes([
			    PACKAGE_PATH_CMS . 'public\css\manager.css' => public_path('vendor/ecommerce/css/manager.css'),
			    PACKAGE_PATH_CMS . 'public\js' => public_path('vendor/ecommerce/js'),
		    ], 'public');

		    $this->mergeConfigFrom(PACKAGE_PATH_CMS . 'config' . DIRECTORY_SEPARATOR . 'database.php', 'database');

		    $this->loadRoutesFrom(PACKAGE_PATH_CMS . 'routes' . DIRECTORY_SEPARATOR . 'manager.php');

		    $this->loadViewsFrom(PACKAGE_PATH_CMS . 'views', 'manager');
		    # exmaple return view('manager::admin');

		    #$this->loadTranslationsFrom('', 'manager');
		    # example echo trans('manager::messages.welcome');

		    $this->loadMigrationsFrom(MIGRATIONS_CMS);

		    $this->commands([
			    'CMS\Console\Commands\MakeShop', 'CMS\Console\Commands\PatchShops',
			    'CMS\Console\Commands\MigrateMaster', 'CMS\Console\Commands\SetupCMS'
		    ]);

		    // Create constant template vars
//		    app('view')->composer('manager::layouts.app', function ($view)
//		    {
//			    $view->with(['key' => $value]);
//		    });

		    Switcher::master();

	    }

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
	    /*
		|--------------------------------------------------------------------------
		| CMS Only
		|--------------------------------------------------------------------------
		|
		| Only run if the current request url root matches the main configured application url
	    | See APP_URL in .env or 'url' in root/config/app.php
		|
		*/

	    if($this->inCms === true)
	    {

	    }
    }

	/**
	 * Override default config merge function from ServiceProvider
	 * Merge the given configuration with the existing configuration.
	 * The values passed to array_merge have been reversed to fix package config override.
	 *
	 * @param string $path
	 * @param string $key
	 * @return void
	 */
	protected function mergeConfigFrom($path, $key)
	{
		$config = require $path;

		$this->app['config']->set($key, array_merge($this->app['config']->get($key, []), $config));
	}
}
